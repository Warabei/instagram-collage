package ru.mikhailyudin.rmrcc;

import android.app.Application;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

/**
 * Created by fuck3r on 17.03.14.
 */
public class CCApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        final ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));
    }
}
