package ru.mikhailyudin.rmrcc.image;

import java.io.Serializable;

/**
 * Created by fuck3r on 18.03.14.
 */
public class Image implements Serializable {
    private final String lowResUrl;
    private final String thumbnailUrl;
    private final String standardResolutionUrl;

    public boolean isChecked = true;

    public Image(final String lowResUrl, final String thumbnailUrl, final String standardResolutionUrl) {
        this.lowResUrl = lowResUrl;
        this.thumbnailUrl = thumbnailUrl;
        this.standardResolutionUrl = standardResolutionUrl;
    }

    public String getLowResUrl() {
        return lowResUrl;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public String getStandardResolutionUrl() {
        return standardResolutionUrl;
    }
}
