package ru.mikhailyudin.rmrcc.image;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.widget.FrameLayout;

/**
 * Created by fuck3r on 18.03.14.
 */
public class ImageLayout extends FrameLayout {
    public ImageLayout(final Context context) {
        super(context);
    }

    public ImageLayout(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    public ImageLayout(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
    }

    public Bitmap getBitmap(){
        this.setDrawingCacheEnabled(true);
        this.buildDrawingCache();
        Bitmap bmp = Bitmap.createBitmap(this.getDrawingCache());
        this.setDrawingCacheEnabled(false);
        return bmp;
    }

}
