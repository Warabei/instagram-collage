package ru.mikhailyudin.rmrcc.image;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.nostra13.universalimageloader.core.ImageLoader;
import ru.mikhailyudin.rmrcc.R;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: fuck3r
 * Date: 28.02.14
 * Time: 23:13
 * To change this template use File | Settings | File Templates.
 */
public class ImageArrayAdapter extends ArrayAdapter<Image> {

    private final LayoutInflater inflater;

    public ImageArrayAdapter(final Context context, final List<Image> images) {
        super(context, 0, images);
        inflater = LayoutInflater.from(getContext());
    }

    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {
        final View checkedImageView;
        final ViewHolder viewHolder;
        if (convertView == null) {
            checkedImageView = inflater.inflate(R.layout.item_checked_image_view, null);
            viewHolder = new ViewHolder(
                    (ImageView) checkedImageView.findViewById(R.id.image),
                    (CheckBox) checkedImageView.findViewById(R.id.chk_box)
            );
            checkedImageView.setTag(viewHolder);
        } else {
            checkedImageView = convertView;
            viewHolder = (ViewHolder) checkedImageView.getTag();
        }
        final ImageView imageView = viewHolder.imageView;
        final CheckBox checkBox = viewHolder.checkBox;
        final Image imageInfo = this.getItem(position);
        checkBox.setChecked(imageInfo.isChecked);

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked) {
                imageInfo.isChecked = isChecked;
            }
        });

        ImageLoader.getInstance().displayImage(imageInfo.getThumbnailUrl(), imageView);
        return checkedImageView;
    }

    private static class ViewHolder{
        private final ImageView imageView;
        private final CheckBox checkBox;

        private ViewHolder(final ImageView imageView, final CheckBox checkBox) {
            this.imageView = imageView;
            this.checkBox = checkBox;
        }
    }

}
