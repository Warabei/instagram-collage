package ru.mikhailyudin.rmrcc.activity;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.GridView;
import ru.mikhailyudin.rmrcc.R;
import ru.mikhailyudin.rmrcc.image.Image;
import ru.mikhailyudin.rmrcc.image.ImageArrayAdapter;

import java.io.Serializable;
import java.util.List;

/**
 * Created by fuck3r on 18.03.14.
 */
public class ImageCheckActivity extends ActionBarActivity {
    private List <Image> images;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check);
        GridView gridView = (GridView) findViewById(R.id.image_grid_view);
        if (savedInstanceState == null){
           images = (List<Image>) getIntent().getSerializableExtra(CollageCreatorActivity.IMAGES);
        }
        else{
           images = (List<Image>) savedInstanceState.getSerializable(CollageCreatorActivity.IMAGES);
        }

        gridView.setAdapter(new ImageArrayAdapter(this, images));
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK, new Intent().putExtra(CollageCreatorActivity.IMAGES, getIntent().getSerializableExtra("arr")));
        super.onBackPressed();
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(CollageCreatorActivity.IMAGES, (Serializable) images);
    }
}
