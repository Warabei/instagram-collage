package ru.mikhailyudin.rmrcc.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.nostra13.universalimageloader.core.ImageLoader;
import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import ru.mikhailyudin.rmrcc.R;
import ru.mikhailyudin.rmrcc.image.Image;
import ru.mikhailyudin.rmrcc.image.ImageLayout;
import ru.mikhailyudin.rmrcc.view.GestureImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class CollageCreatorActivity extends ActionBarActivity implements SearchView.OnQueryTextListener {

    public static final int REQUEST_CODE = 1337;
    public static final String IMAGES = "arr";
    private ImageLayout content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collage_creator);
        content = (ImageLayout) findViewById(R.id.content_frame);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.mi_add_item) {
            new SendImageTask(this, getExternalCacheDir()).execute(content.getBitmap());
            return true;
        }
        return false;
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            List<Image> images = (List<Image>) data.getSerializableExtra(IMAGES);
            for (Image image : images) {
                final GestureImageView view = new GestureImageView(this);
                final FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT);
                view.setScaleType(ImageView.ScaleType.MATRIX);
                ImageLoader.getInstance().displayImage(image.getStandardResolutionUrl(), view);
                view.setLayoutParams(params);
                content.addView(view);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.m_collage_creator, menu);
        getMenuInflater().inflate(R.menu.main, menu);

        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setQueryHint("Поиск");
        searchView.setOnQueryTextListener(this);
        searchView.setIconifiedByDefault(false);
        searchView.setQuery("", false);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(final String s) {
        new InstagramSearchTask(this).execute(s);
        return true;
    }

    @Override
    public boolean onQueryTextChange(final String s) {
        return false;
    }

    private static class InstagramSearchTask extends AsyncTask<String, Void, List<Image>> {

        private final Activity activity;
        private ProgressDialog progressDialog;

        private InstagramSearchTask(final Activity activity) {
            this.activity = activity;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(activity);
            progressDialog.show();

        }

        @Override
        protected void onPostExecute(final List<Image> images) {
            progressDialog.dismiss();
            if (!images.isEmpty()){
                activity.startActivityForResult(new Intent(activity, ImageCheckActivity.class).putExtra(IMAGES,
                    (Serializable) images), REQUEST_CODE);
            } else {
                Toast.makeText(activity, "Something goes wrong.", Toast.LENGTH_LONG).show();
            }

        }

        @Override
        protected List<Image> doInBackground(final String... params) {
            final List<Image> images = new ArrayList<Image>();
            try {
                return getImages(getUserId(getImagesJson(params[0])));
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return images;
        }

        private List<Image> getImages(String userId) throws IOException {
            String response = getNetworkResponse("https://api.instagram" +
                    ".com/v1/users/" + userId + "/media/recent/?client_id=4d03a38607c8439a8742f91fe79212db&count=10");
            JsonArray jsonArray = new JsonParser().parse(response).getAsJsonObject().getAsJsonArray("data");
            List<Image> images = new ArrayList<Image>();
            if (jsonArray.size() == 0) {
                return null;
            }
            for (int i = 0; i < jsonArray.size(); i++) {
                JsonObject imageJson = jsonArray.get(i).getAsJsonObject();
                imageJson = imageJson.getAsJsonObject("images");
                images.add(new Image(
                        imageJson.getAsJsonObject("low_resolution").get("url").getAsString(),
                        imageJson.getAsJsonObject("thumbnail").get("url").getAsString(),
                        imageJson.getAsJsonObject("standard_resolution").get("url").getAsString()
                ));

            }
            return images;

        }

        private String getUserId(String userResponse) throws JSONException {
            JsonArray jsonArray = new JsonParser().parse(userResponse).getAsJsonObject().getAsJsonArray("data");
            if (jsonArray.size() == 0) {
                return null;
            }
            return jsonArray.get(0).getAsJsonObject().get("id").getAsString();
        }

        private String getImagesJson(String query) throws IOException {
            return getNetworkResponse("https://api.instagram" +
            ".com/v1/users/search?q=" + Uri.encode(query) +
                    "&client_id=4d03a38607c8439a8742f91fe79212db&count=1");
        }

        private String getNetworkResponse(String query) throws IOException {
            URL url = new URL(query);
            URLConnection urlConnection = url.openConnection();
            urlConnection.setConnectTimeout(5000);
            urlConnection.setReadTimeout(10000);
            return IOUtils.toString(urlConnection.getInputStream(), "UTF-8");
        }
    }

    private static class SendImageTask extends AsyncTask<Bitmap, Void, String> {

        private final Activity activity;
        private final File folder;

        private SendImageTask(final Activity activity, final File folder) {
            this.activity = activity;
            this.folder = folder;
        }

        @Override
        protected String doInBackground(final Bitmap... params) {
            final File file = new File(folder, "canvas.jpg");
            Bitmap bitmap = params[0];
            FileOutputStream out = null;
            try {
                out = new FileOutputStream(file.getPath());
                if (bitmap != null) {
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (Exception e) {
                    Log.e("fileSavingError", e.getMessage());
                }
            }
            return file.getPath();
        }

        @Override
        protected void onPostExecute(final String path) {
            Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
            emailIntent.setType("application/image");
            emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"robot@yandex.ru"});
            emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject");
            emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "from Collage Creator");
            emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + path));
            activity.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
        }
    }
}
